// jest.config.js
// Sync object
module.exports = {
  verbose: true,
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.js'
  ],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 95,
      lines: 95,
      statements: 95
    }

  },
  coverageReporters: ['text', 'json', 'lcov'],
  resetMocks: true
}
