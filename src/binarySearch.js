function binarySearch (numberArray, numberToBeFound) {
  if (!Number.isInteger(numberToBeFound)) {
    return 'provide a valid number to be found'
  }
  for (let it = 0; it < numberArray.length; it++) {
    if (!Number.isInteger(numberArray[it])) {
      return 'provide a number array'
    }
  }

  let startIndex = 0; let endIndex = numberArray.length - 1

  while (startIndex <= endIndex) {
    const midIndex = Math.floor((startIndex + endIndex) / 2)

    if (numberArray[midIndex] === numberToBeFound) return midIndex

    else if (numberArray[midIndex] < numberToBeFound) { startIndex = midIndex + 1 } else { endIndex = midIndex - 1 }
  }

  return -1
}

module.exports = binarySearch
