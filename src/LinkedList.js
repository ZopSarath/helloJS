class Node { 
    constructor(item) 
    { 
        this.element = item; 
        this.next = null
    } 
} 


class LinkedList { 
    constructor() 
    { 
        this.head = null;  
    } 
  
    add(item) 
    { 
        let node = new Node(item); 
  
        let current; 
  
        if (this.head == null) 
            this.head = node; 
        else { 
            current = this.head; 
  
            while (current.next) { 
                current = current.next; 
            } 
            
            current.next = node; 
        } 
    }
    
    removeFromBack() 
    { 
        let current; 

        current = this.head; 
  
        while (current.next.next) { 
            current = current.next; 
        } 
            
        current.next = null; 
             
    } 

    removeElement(item)
    {
        let current

        current = this.head;

        while(current.next.element != item)
        {
            current = current.next;
        }

        if(current.next.next)
            current.next = current.next.next;
        else
            current.next = null;
    }
} 

let list1 = new LinkedList;
list1.head = new Node("Mon")
e2 = new Node("Tue")
e3 = new Node("Wed")

list1.head.next = e2

e2.next = e3

list1.add("Hello");
list1.add("JS")


const assert = require('assert').strict;

assert.deepEqual(list1.head.element,"Mon")
assert.deepEqual(list1.head.next.element,e2.element);
assert.deepEqual(e2.next.element,e3.element);
assert.deepEqual(list1.head.next.next.element,e3.element);
assert.deepEqual(e3.next.element,'Hello');
assert.deepEqual(e3.next.next.element,'JS');

list1.removeFromBack();

assert.deepEqual(e3.next.next,null);

list1.removeElement("Wed");

assert.deepEqual(list1.head.next.next.element,'Hello');

//no assertion errors thrown