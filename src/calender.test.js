const noOfDays = require('./calender')

describe('should return number of days', () => {
  test.each([
    ['should return 7500', '01-feb-2000', '03-sep-2020', 7500],
    ['should return 9625', '07-jan-1900', '08-nov-1926', 9625],
    ['should return 27', '22-jan-2020', '17-feb-2020', 27],
    ['should return 238', '14-mar-2020', '06-nov-2020', 238],
    ['should return 251', '22-feb-2020', '30-oct-2020', 251],
    ['should return 476', '13-apr-2020', '02-aug-2021', 476],
    ['should return 865', '05-apr-2020', '23-aug-2022', 865],
    ['should return 945', '10-jan-2020', '23-aug-2022', 945]

  ])('%s', (_unusedVar, startDate, endDate, result) => {
    const response = noOfDays(startDate, endDate)
    expect(response).toBe(result)
  })
})
