const compareTriplets = require('./compareTriplets');

describe('should compare the Triplets', () => {
    
    test.each([

        ['should return [1,1]',[1,2,3],[3,2,1],[1,1]],
        ['should return [1,1]',[5,6,7],[3,6,10],[1,1]]

    ])('%s',(_unusedVar,tripletA,tripletB,scores) => {
        expect(compareTriplets(tripletA,tripletB)).toStrictEqual(scores);
    })


})


