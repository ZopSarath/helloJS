function isValid (s) {
  const counter = {}
  const freq = {}

  Array.from(s).forEach(char => {
    counter[char] = counter[char] || 0
    counter[char]++
  })

  Object.keys(counter).forEach(k => {
    freq[counter[k]] = freq[counter[k]] || 0
    freq[counter[k]]++
  })

  const freqArr = Object.keys(freq).map(Number)

  if (freqArr.length === 1) {
    return 'YES'
  }

  const twoFrequencies = freqArr.length === 2
  const [a, b] = freqArr

  const oneOccurrence = freq[a] === 1 || freq[b] === 1

  const singleton = (freq[a] === 1 && a === 1) || (freq[b] === 1 && b === 1)

  const diffOfOne = (freq[a] === 1 ? a - b : b - a) === 1

  if ((singleton || diffOfOne) && twoFrequencies && oneOccurrence) {
    return 'YES'
  }

  return 'NO'
}

module.exports = isValid
