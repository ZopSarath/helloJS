const isValid = require('./sherlockAndTheValidString');

describe('should find out if the string is valid', () => {

    test.each([

        ['should return YES','aabbcc','YES'],
        ['should return NO','aabbccddeefghi','NO'],
        ['should return NO','aabbcd','NO'],
        ['should return NO','abcdefghhgfedecba','YES']


    ])('%s',(_unusedVar,string,result) => {
    expect(isValid(string)).toBe(result);
    })
})