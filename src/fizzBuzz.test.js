const fizzBuzz = require('./fizzBuzz')

describe('Should test fizzBuzz', () => {
  test('should console log  FizzBuzz for 15', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation()
    const number = 15
    fizzBuzz(number)
    expect(spy).toHaveBeenCalledTimes(15)
    expect(spy.mock.calls[14][0]).toEqual('FizzBuzz')
  })
  test('should console log Fizz for 33', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation()
    const number = 50
    fizzBuzz(number)
    expect(spy).toHaveBeenCalledTimes(50)
    expect(spy.mock.calls[32][0]).toEqual('Fizz')
  })
  test('should console log type a number for invalid number', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation()
    const number = '!'
    fizzBuzz(number)
    expect(spy).toHaveBeenCalledTimes(1)
    expect(spy.mock.calls[0][0]).toEqual('type a number')
  })
})
