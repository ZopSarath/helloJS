const fs = require('fs')
const superagent = require('superagent')

// utility functions:
function generateRandomString (length) {
  let result = ''
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

function requestImage (string) {
  return superagent.get(`https://robohash.org/${string}`)
}

// promises:
const stringGenerationPromise = (length) => {
  return new Promise((resolve, reject) => {
    resolve(generateRandomString(length))
  })
}

const imageRequestPromise = (string) => {
  return new Promise((resolve, reject) => {
    resolve(requestImage(string))
  })
}

const writeFilePromise = (data, fileName) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, data, (error) => {
      if (error) reject(error)
      resolve()
    })
  })
}

// consuming promises
stringGenerationPromise(length).then((string) => {
  console.log(string)
  return imageRequestPromise(string)
}).then((data) => {
  console.log(data.body)
  return writeFilePromise(data.body, 'robo.png')
}).then(() => {
  console.log('task succesfully completed')
}).catch((error) => {
  console.log(error)
})

module.exports = stringGenerationPromise
