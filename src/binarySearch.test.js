const binarySearch = require('./binarySearch')

describe('should find the elements in an array', () => {
  test.each([
    ['should return 0', [1, 2, 3, 4, 5, 6], 1, 0],
    ['should return 1', [1, 2, 3, 4, 5, 6], 2, 1],
    ['should return 2', [1, 2, 3, 4, 5, 6], 3, 2],
    ['should return 3', [1, 2, 3, 4, 5, 6], 4, 3],
    ['should return 4', [1, 2, 3, 4, 5, 6], 5, 4],
    ['should return 5', [1, 2, 3, 4, 5, 6], 6, 5],
    ['should return -1', [1, 2, 3, 4, 5, 6], 7, -1],
    ['should return -1', [1, 2, 3, 4, 5, 6], 8, -1],
    ['should throw an error message', [5, 6, 7, 8], '@', 'provide a valid number to be found'],
    ['should throw an error message', ['hello'], 1, 'provide a number array']
  ])('%s', (_unused, numberArray, elementToBeFound, result) => {
    const response = binarySearch(numberArray, elementToBeFound)
    expect(response).toBe(result)
  })
})
