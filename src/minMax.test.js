const miniMaxSum = require('./minMax')

describe('should give the min and max sums', () => {
  test.each([

    ['should return [15,20]', [1, 2, 3, 4, 5, 6], [15, 20]],
    ['should return [15,20]', [2, 5, 4, 6, 3, 1], [15, 20]],
    ['should return [49,111]', [8, 9, 10, 22, 70], [49, 111]]

  ])('%s', (_unusedVar, array, result) => {
    expect(miniMaxSum(array)).toStrictEqual(result)
  })
})
