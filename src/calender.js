function checkLeapYear (year) {
  if (year % 400 === 0) return true
  if (year % 100 === 0) return false
  if (year % 4 === 0) return true
  return false
}

function noOfDays (startDate, endDate) {
  const monthsIndexes = {
    jan: 1,
    feb: 2,
    mar: 3,
    apr: 4,
    may: 5,
    jun: 6,
    jul: 7,
    aug: 8,
    sep: 9,
    oct: 10,
    nov: 11,
    dec: 12
  }

  const daysInTheMonths = {
    jan: 31,
    feb: 27,
    mar: 31,
    apr: 30,
    may: 31,
    jun: 30,
    jul: 31,
    aug: 31,
    sep: 30,
    oct: 31,
    nov: 30,
    dec: 31
  }

  const daysInTheMonthsValues = Object.values(daysInTheMonths)

  const startDay = parseInt(startDate.substring(0, 2))
  const endDay = parseInt(endDate.substring(0, 2))

  const startMonth = startDate.substring(3, 6)
  const endMonth = endDate.substring(3, 6)

  const startYear = parseInt(startDate.substring(7, 11))
  const endYear = parseInt(endDate.substring(7, 11))

  let countDays = 0

  if (startYear === endYear) {
    for (let iterator = monthsIndexes[startMonth]; iterator <= monthsIndexes[endMonth]; iterator++) {
      if (iterator === monthsIndexes[startMonth]) {
        countDays = countDays + (daysInTheMonthsValues[iterator - 1] - startDay) + 1
      } else if (iterator === monthsIndexes[endMonth]) {
        countDays = countDays + endDay
      } else { countDays = countDays + daysInTheMonthsValues[iterator - 1] }
    }

    if (monthsIndexes[startMonth] <= 2 && monthsIndexes[endMonth] > 2) {
      if (checkLeapYear(startYear)) {
        countDays++
      }
    }
  } else if (startYear < endYear) {
    for (let iterator = monthsIndexes[startMonth]; iterator <= 12 * (endYear - startYear); iterator++) {
      let monthIndex = 0
      if (iterator < 12) { monthIndex = iterator % 12 } else if (iterator % 12 === 0) {
        monthIndex = 12
      } else { monthIndex = iterator % 12 }

      if (monthIndex === monthsIndexes[startMonth]) {
        countDays = countDays + (daysInTheMonthsValues[monthIndex - 1] - startDay) + 1
      } else { countDays = countDays + daysInTheMonthsValues[monthIndex - 1] }
    }

    for (let iterator = 1; iterator <= monthsIndexes[endMonth]; iterator++) {
      if (iterator === monthsIndexes[endMonth]) {
        countDays = countDays + endDay
      } else { countDays = countDays + daysInTheMonthsValues[iterator - 1] }
    }

    for (let iterator = startYear; iterator <= endYear; iterator++) {
      if (iterator === startYear) {
        if (checkLeapYear(iterator) && monthsIndexes[startMonth] <= 2) {
          countDays++
        }
      } else if (iterator === endYear) {
        if (checkLeapYear(endYear) && monthsIndexes[endMonth] > 2) {
          countDays++
        }
      } else {
        if (checkLeapYear(iterator)) {
          countDays++
        }
      }
    }
  }

  return countDays
}

module.exports = noOfDays
