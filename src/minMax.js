// find minimum and maximum sums in an array
function miniMaxSum (array) {
  let min = +Infinity; let max = -Infinity; let sum = 0
  for (let i = 0; i < array.length; i++) {
    sum += array[i]
    if (array[i] < min) { min = array[i] }
    if (array[i] > max) { max = array[i] }
  }
  return [sum - max, sum - min]
}

module.exports = miniMaxSum
